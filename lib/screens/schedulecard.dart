import 'package:flutter/material.dart';

class ScheduleCard extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Container(
          padding: EdgeInsets.all(8),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.start,
            mainAxisSize: MainAxisSize.min,
            children: [
              Text(
                "Your Schedule",
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontFamily: 'SF Pro Text',
                ),
              ),
              Text(
                "Upcoming Classes & Tasks",
                style: TextStyle(
                  fontWeight: FontWeight.normal,
                  fontFamily: 'SF Pro Text',
                ),
              ),
            ],
          ),
        ),
        Container(
          padding: EdgeInsets.all(20),
          decoration: BoxDecoration(
            color: Colors.lightBlueAccent,
            borderRadius: BorderRadius.circular(20),
          ),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.start,
            mainAxisSize: MainAxisSize.min,
            children: [
              Text(
                "Physics",
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontFamily: 'SF Pro Text',
                  color: Colors.white,
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(bottom: 30.0),
                child: Text(
                  "Chapter 3 Force",
                  style: TextStyle(
                    fontWeight: FontWeight.normal,
                    fontFamily: 'SF Pro Text',
                    color: Colors.white,
                  ),
                ),
              ),
              Row(
                children: [
                  Icon(
                    Icons.access_time,
                    color: Colors.white,
                  ),
                  SizedBox(width:10),
                   // Add some space between icon and text
                  Text(
                    "09:30",
                    style: TextStyle(
                      fontWeight: FontWeight.normal,
                      fontFamily: "SF Pro Text",
                      color: Colors.white,
                    ),
                  ),
                ],
              ),
              Row(
                children: [
                  Icon(
                    Icons.people_alt,
                    color: Colors.deepOrange,
                  ),
                  SizedBox(width:10),
                  // Add some space between icon and text
                  Text(
                    "Alex Jesus",
                    style: TextStyle(
                      fontWeight: FontWeight.normal,
                      fontFamily: "SF Pro Text",
                      color: Colors.white,
                    ),
                  ),
                ],
              ),
              Row(
                children: [

                  Icon(
                    Icons.link,
                    color: Colors.green,
                  ),
                  SizedBox(width:10),
                  // Add some space between icon and text
                  Text(
                    "Google Meet",
                    style: TextStyle(
                      fontWeight: FontWeight.normal,
                      fontFamily: "SF Pro Text",
                      color: Colors.white,
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
      ],
    );
  }
}