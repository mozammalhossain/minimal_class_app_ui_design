import 'package:flutter/material.dart';
import 'package:ui_profile/screens/running_subjects_scrollview.dart';
import 'package:ui_profile/screens/schedulecard.dart';
import 'package:ui_profile/style/assetmanager.dart';

class HomeScreenBody extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    // Your body content goes here
    // return Scaffold(
    //   body: Padding(
    //     padding: const EdgeInsets.all(20.0),
    //     child: Row(
    //       mainAxisAlignment: MainAxisAlignment.spaceBetween,
    //       crossAxisAlignment: CrossAxisAlignment.start,
    //       children: [
    //         const Column(
    //           crossAxisAlignment: CrossAxisAlignment.start,
    //           children: [
    //             Text(
    //               'Hello Lina',
    //               style: TextStyle(
    //                 fontSize: 18, // Adjust the font size as needed
    //                 fontFamily: 'SF Pro Text',
    //               ),
    //             ),
    //             //SizedBox(height: 10),
    //             Text(
    //               "You've got",
    //               style: TextStyle(
    //                 fontSize: 20, // Adjust the font size as needed
    //                 fontFamily: 'SF Pro Text',
    //                 fontWeight: FontWeight.bold,
    //               ),
    //             ),
    //             //SizedBox(height: 10),
    //             Text(
    //               '4 Tasks Today',
    //               style: TextStyle(
    //                 fontSize: 24, // Adjust the font size as needed
    //                 fontFamily: 'SF Pro Text',
    //                 fontWeight: FontWeight.bold,
    //                 color: Colors.deepOrange,
    //               ),
    //             ),
    //           ],
    //
    //         ),
    //         Image.asset(
    //           assetmanager.profile,
    //         ),
    //       ],
    //     ),
    //   ),
    // );
    return Scaffold(
      body: Container(
        padding: const EdgeInsets.all(20.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      'Hello Lina',
                      style: TextStyle(
                        fontSize: 18, // Adjust the font size as needed
                        fontFamily: 'SF Pro Text',
                      ),
                    ),
                    Text(
                      "You've got",
                      style: TextStyle(
                        fontSize: 20, // Adjust the font size as needed
                        fontFamily: 'SF Pro Text',
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                    Text(
                      '4 Tasks Today',
                      style: TextStyle(
                        fontSize: 24, // Adjust the font size as needed
                        fontFamily: 'SF Pro Text',
                        fontWeight: FontWeight.bold,
                        color: Colors.deepOrange,
                      ),
                    ),
                  ],
                ),
                Image.asset(
                  assetmanager.profile,
                ),
              ],
            ),
            // Additional containers go here
            Container(
              padding: EdgeInsets.only(top: 20.0),
              // margin: EdgeInsets.symmetric(vertical: 10),
              // color: Colors.blue,
              child: Text(
                'Courses',
                style: TextStyle(
                  fontSize: 24,
                  fontFamily: 'SF Pro Text',
                  color: Colors.black,
                  fontWeight: FontWeight.bold,
                ),
              ),



            ),
            Container(
              padding: EdgeInsets.only(top: 1.0),
              // margin: EdgeInsets.symmetric(vertical: 10),
              // color: Colors.blue,
              child: Text(
                'Your running subjects',
                style: TextStyle(
                  fontSize: 19,
                  fontFamily: 'SF Pro Text',
                  color: Colors.black,
                  fontWeight: FontWeight.w300,
                ),
              ),



            ),
            Runningsubjectsscrollview(),
            ScheduleCard(),

                     // Add more containers as needed
          ],
        ),
      ),
    );
  }
}
